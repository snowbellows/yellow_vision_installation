# Yellow Vision

## Installing Prerequisites for Development

### Install Rust

<https://rustup.rs/>

### Install OpenCV

#### Install OpenSSL Tools

`sudo apt-get install libssl-dev`

#### Install Cmake

<https://vitux.com/how-to-install-cmake-on-ubuntu-18-04/>

```zsh
cd ~/Desktop
wget https://github.com/Kitware/CMake/releases/download/v3.16.3/cmake-3.16.3.tar.gz
tar -zxvf cmake-3.16.3.tar.gz
cd cmake-3.16.3
./bootstrap
make
sudo make install
```

#### Install Other Dependencies

```zsh
sudo apt-get install libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev
```

#### Install OpenCV from source

```zsh
cd ~/Desktop
wget https://github.com/opencv/opencv/archive/4.2.0.zip
unzip 4.2.0.zip
mkdir ~/opencv
cd ~/opencv
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local -DOPENCV_GENERATE_PKGCONFIG=ON ~/Desktop/opencv-4.2.0
make -j7 # runs 7 jobs in parallel
sudo make install
```
